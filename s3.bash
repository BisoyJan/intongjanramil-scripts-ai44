#!/bin/sh

echo "Enter name of the Folder"
read Dir

echo "Enter how many copies"
read Copies

for (( c=1; c<=$Copies; c++ ))
do

if [[ $c -eq 1 ]]
then
    mkdir $Dir
else
    mkdir $Dir$c
fi
done
